import sys
try:
    numToConvert = sys.argv[1]
    int(sys.argv[1])
except (IndexError, ValueError):
    print("Error: pass a number")
    sys.exit()

significants = [
    "",
    "hundred",
    "thousand",
    "million",
    "billion",
    "trillion",
    "quadrillion",
    "quintillion",
    "sextillion",
    "septillion",
    "octillion",
    "nonillion",
    "decillion",
    "undecillion",
    "duodecillion",
    "tredecillion",
    "quattuordecillion",
    "quindecillion",
    "sexdecillion",
    "septendecillion",
    "octodecillion",
    "novemdecillion",
    "vigintillion",
    "centillion"
]

numberList = [
    "zero",
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
]

tens = [
    ["","ten"],
    ["", "eleven"],
    ["twen", "twelve"],
    "thir",
    "four",
    "fif",
    "six",
    "seven",
    "eigh",
    "nine"
]

def formWord(num, place):
    if num[place] == 0:
        return ""

    if place == 0:
        try:
            if num[1] == 1:
                return ""
        except IndexError:
            pass

        return numberList[num[place]]

    if place == 1:
            if num[place] == 1:
                teenWord = tens[num[place-1]]
                if len(teenWord[1]) > 1:
                    return teenWord[1]
                else:
                    return teenWord + "teen"
            else:
                teenWord = tens[num[place]]
                if type(teenWord) == list:
                    teenWord = tens[num[place]][0]
                return teenWord + "ty"

    return str(significants[1]) +  " " +  str(numberList[num[place]])

def groupNums(numberArray):
    numberSets = []
    tempArr = []
    for x in numberArray:
        tempArr.append(x)
        if len(tempArr) == 3:
            numberSets.append(tempArr)
            tempArr = []

    if len(tempArr) > 0:
        numberSets.append(tempArr)

    return numberSets

def insertAnd(numWord):
    numWord = numWord.strip().split(" ")
    try:
        del numWord[numWord.index("")]
    except ValueError as e:
        pass

    if len(numWord) > 2:
        if "ty" in numWord[1]:
            numWord.insert(2, "and")
        else:
            numWord.insert(1, "and")

    return " ".join(numWord)

def numToWord(number):
    # converts the number into an array, each digit being its own element
    # we also reverse this
    numberGroups = groupNums([int(x) for x in list(number)][::-1])
    numWord = ""

    if len(numberGroups) == 1 and len(numberGroups[0]) == 1 and numberGroups[0][0]  == 0:
        return numberList[0]

    sigAmount = -1
    for index, currentGroup in enumerate(numberGroups):
        numWordGroup = ""
        sigAmount += 1
        if sigAmount != 0:
            numWord += " " + significants[sigAmount+1]
        for i in range(len(currentGroup)):
            numWordGroup += " "  + formWord(currentGroup, i)

        numWord += " " + insertAnd(numWordGroup)

        # Working out if we need an and between number groups
        currentGroupLen = len(currentGroup)
        ones = currentGroup[0]
        tens = 0
        hundreds = 0
        if currentGroupLen > 1:
            tens = currentGroup[1]

        if currentGroupLen == 3:
            hundreds = currentGroup[2]

        if index != len(numberGroups) - 1 and hundreds == 0 and len(numberGroups) > 1 and (ones != 0 or tens != 0):
            numWord += " and"

    return " ".join(numWord.strip().split(" ")[::-1])

print(f"Your number is: {numToWord(numToConvert)}")
