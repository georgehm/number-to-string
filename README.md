# Number to string
- Converts a number to it's word form e.g. 12345 -> twelve thousand three hundred and forty five

## Usage:
- python3 num_to_word.py 123
